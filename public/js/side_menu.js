$(document).ready(function(){

// Initialize collapse button
$(".button-collapse").sideNav();
$(".button-collapse").click(function(){
    $("#slide-out .nav-links li").each(function(){
        $(this).addClass("animated slideInLeft");
    });
});

$('.modal-trigger').leanModal();

$('.collapsible').collapsible({
      accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
  });

$(".button").click(function() {
  $(this).parent().toggleClass("col m12");
});     

});


/*function toggleClass(el){
    var kids = document.getElementById('panel-title').children;
    for(var i = 0; i < kids.length; i++){
        kids[i].className = "col m4 s12";
    }
    el.className = "col m12 s12";
}*/


$(".task-panel .trigger").bind("click",function(){
    var small = 'col m4 s12';
    var large = 'col m12 s12';

    //$(this).attr("data-bool",1);
    var data_bool = $(this).attr("data-bool");
    //$(this).attr("data-bool",1);

    $(".task-panel").removeClass(large).addClass(small);
    $(".trigger").not(this).attr("data-bool",0);

    var this_parent = $(this).parents().closest(".task-panel");

    if(data_bool == 1){
        $(this_parent).removeClass(large).addClass(small);
        $(this).attr("data-bool",0);
    }
    else{
        $(this_parent).removeClass(small).addClass(large);
        $(this).attr("data-bool",1);
    }
});

//for sidebar closing button
$("#side-nav-clear").on("click", function() {
    $("#sidenav-overlay").trigger("click");
    return false;
});

//

