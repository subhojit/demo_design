<!DOCTYPE html>
<html>
<head>
  <title>Dashboard</title>
  <link href="{{ asset('vendor/materialize/css/materialize.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('vendor/animate.css') }}" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
  rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/angular-material-icons/0.7.1/angular-material-icons.min.js"></script> 

</head>
<body>

  <nav class="blue lighten-2">
    <div>
      <ul id="slide-out" class="side-nav">
       <a class="right-align" id="side-nav-clear">
         <i class="large material-icons blue-text lighten-3">swap_horiz</i>
       </a>
       <li>
        <div class="userView">
          <img class="background"  src="images/office.jpg">
          <a href="#!user"><img class="circle " src="images/yuna.jpg"></a>

          <a href="#!name"><span class="white-text name">John Doe</span></a>
          <a href="#!email"><span class="white-text email">jdandturk@gmail.com</span></a>
        </div>
      </li>
      <section class="nav-links">
        <li><a class="waves-effect blue-text" href="#!"><i class="material-icons">receipt</i>Home Links</a></li>
        <li><a class="waves-effect blue-text" href="#!"><i class="material-icons">shopping_cart</i>Home Links</a></li>
        <li><a class="waves-effect blue-text" href="#!"><i class="material-icons">cloud</i>Home Links</a></li>
        <li><a class="waves-effect blue-text" href="#!"><i class="material-icons">library_music</i>Home Links</a></li>
        <li><a class="waves-effect blue-text" href="#!"><i class="material-icons">star</i>Home Links</a></li>
        <li><a class="waves-effect blue-text" href="#!"><i class="material-icons">business</i>Home Links</a></li>
        <li><a class="waves-effect blue-text" href="#!"><i class="material-icons">call</i>Home Links</a></li>
      </section>


      <li><div class="divider"></div></li>
      <li><a class="subheader">Subheader</a></li>

    </ul>
    <a href="#" data-activates="slide-out" class="button-collapse"><i class="medium material-icons">menu</i></a>
    <div href="#" class="brand-logo">Dashboard</div>
    <ul   class="right  ">



      <ul id="dropdown2" class="dropdown-content ">
        <li><a class="blue-text" href="#!">one<span class="badge blue-text">1</span></a></li>
        <li><a class="blue-text" href="#!">two<span class="new badge red">1</span></a></li>
        <li><a class="blue-text" href="#!">three</a></li>
      </ul>


      <a class=" dropdown-button" href="#!" data-activates="dropdown2"><i class="material-icons">more_vert</i></a> 
    </ul>
  </div>
</nav>